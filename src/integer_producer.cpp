#include <ros/ros.h>
#include <std_msgs/Int32.h>
#include <cstdlib>
#include <ctime>

int main(int argc, char *argv[])
{
    // `integer_producer_node` is the name of our node
    ros::init(argc, argv, "integer_producer_node");

    // Handle to our node
    ros::NodeHandle nh;

    // Advertise our `integer` topic and our queue size is 50.
    // ROS puts the generated integers into a queue and then publishes them
    // from there.
    ros::Publisher integer_pub = nh.advertise<std_msgs::Int32>("integer", 50);

    // Our node works periodically.
    ros::Rate loop_rate(1.0);

    // Default param values.
    int min_value = 0;
    int max_value = 20;

    srand(time(NULL));

    while (ros::ok()) {
        std_msgs::Int32 msg;

        // Node is a namespace for the private parameters accessed by `~`
        //  `/integer_producer_node/min_value` and
        //  `/integer_producer_node/max_value`.
        ros::NodeHandle private_nh("~");

        // If private params don't exists, we use the default values.
        private_nh.param("min_value", min_value, min_value);
        private_nh.param("max_value", max_value, max_value);

        // Generate a random number between [min_value, max_value]
        msg.data = rand() % (max_value - min_value + 1) + min_value;

        // Publish the message
        integer_pub.publish(msg);

        // ROS's own even handling etc...
        ros::spinOnce();

        // Wait for the given loop_rate.
        loop_rate.sleep();
    }
    return 0;
}
