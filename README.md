# ROS 101

## Installing ROS

Get a machine with Ubuntu 16.04 (xenial) installed then follow official [ROS
installation guide for Ubuntu](http://wiki.ros.org/kinetic/Installation/Ubuntu). It is quite a
straight forward process with a bunch of copy-pastes.

## Creating a workspace

A workspace contains one or more packages. In order to build and run the nodes
in a package, you first need a containing workspace.

```bash
$ mkdir -p  ros_training_ws/src # Create a workspace folder and a `src` folder in it
$ cd ros_training_ws/src        # cd into newly created `src` folder
$ catkin_init_workspace         # Initialize our workspace (creates a symlink top-level CMakeLists.txt)
$ cd ..                         # Go back to the root of our workspace
$ catkin_make                   # Creates `build` and `devel` folders in the workspace
$ source devel/setup.bash       # Source our workspace setup to overlay environment variables etc...
$ env | grep ROS                # You should see our newly created workspace overlaid
 ```

## Create a new package in the workspace

You should not create a new package from scratch in this tutorial. The steps
below are given for the sake of completeness. You can safely skip the steps in
this section. I have already created a package for this tutorial; in fact this
README.md is a part of the package.

```bash
$ cd src  # cd into workspace's `src` folder
$ # Create package `integer_analysis` with roscpp, rospy and std_msgs dependecies
$ catkin_create_pkg integer_analysis roscpp rospy std_msgs
$ # Create launch folder in which we will put our *.launch file(s)
$ mkdir integer_analysis/launch
$ # Create scripts folder in which we will put our *.py file(s)
$ mkdir integer_analysis/scripts
```

Generated [package.xml](./package.xml) and [CMakeLists.txt](./CMakeLists.txt)
files are particularly important. They basically contain our package
dependecies and metadata.

## Build the package

Packages contain one or more nodes possibly written in different languages.
Nodes are basically executable units like a program written in C++ or a Python
script.

Let's `git clone` and build it.

```bash
$ cd <your-workspace>/src
$ git clone https://www.gitlab.com/ortax/ros-101.git integer_analysis
$ cd .. # go back to <your-workspace>
$ catkin_make
```

You can build packages with catkin as shown below.
```bash
$ cd <your-workspace>
$ catkin_make   # Build all packages
$ catkin_make <package-name> # Build the package with name <package-name>
```


## Executing and testing out the nodes

### *integer_producer* node

`integer_producer` node generates random integers given a range [a, b]. a
and b are provided as `param`s so that we can change them without modifying the
source code as we will see shortly.

Open a terminal session and start roscore as below. Note that following
commands assume you are in the root folder of our workspace.

```bash
$ source devel/setup.bash
$ roscore
```

Open another terminal session and run our `integer_producer` node as below.

```bash
source devel/setup.bash
# Run integer_producer node which resides in integer_analsis package.
$ rosrun integer_analysis integer_producer
```

Open yet another terminal and list running nodes.

```bash
$ source devel/setup.bash
$ rosnode list
/integer_producer_node # <-- This is our node we have just started.
/rosout                # <-- This is always started by roscore.
```

Let's also list the topics using the last terminal session.

```bash
$ rostopic list
/integer  # <-- Our topic through which we publish random integers.
/rosout
/rosout_agg
```

We can easily see the published integers. We can even change the range of
generated integers using `rosparam` on the fly.

```bash
$ rostopic echo /integer
data: 15
---
data: 18
---
data: 11
---
# CTRL + C to quit

# Update parameters
$ rosparam set /integer_producer_node/max_value 200
$ rosparam set /integer_producer_node/min_value 150

# See the values publish on integer channel again
$ rostopic echo /integer
data: 193
---
data: 189
---
data: 199
# CTRL + C to quit
```

So far we have covered `rosnode`, `rostopic`, `rosparam` commands. They all
have very handly `--help` switch to help you out when you are confused. For
example try out `rostopic --help` on a terminal session. We run our node with
`rosrun` command after running the ros core with `roscore` command in another
terminal session.

Here is how our workspace should look like so far:

```
$ tree ros_training_ws
ros_training_ws
├── devel
│   ├── env.sh
│   ├── lib
│   │   ├── integer_analysis
│   │   │   └── integer_analysis_node
│   │   └── pkgconfig
│   │       └── integer_analysis.pc
│   ├── setup.bash
│   ├── setup.sh
│   ├── _setup_util.py
│   ├── setup.zsh
│   └── share
│       └── integer_analysis
│           └── cmake
│               ├── integer_analysisConfig.cmake
│               └── integer_analysisConfig-version.cmake
└── src
    ├── CMakeLists.txt -> /opt/ros/kinetic/share/catkin/cmake/toplevel.cmake
    └── integer_analysis
        ├── CMakeLists.txt
        ├── include
        │   └── integer_analysis
        ├── launch
        ├── package.xml
        ├── README.md
        ├── scripts
        └── src
            └── integer_producer.cpp
```
